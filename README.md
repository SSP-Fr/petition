Système de pétition on-line pour le SSP Fribourg
================================================

Ce que le projet fait
---------------------
Gestion de multiple pétition en ligne. Indépendamment du CMS qui gère le site.


Pour qui est-il fait
--------------------
Syndicat du Service Public du canton de Fribourg (Suisse).


Sur quels matériel ou plateforme il tourne
-----------------------------------------
* LAMP
	* Linux/GNU
	* Apache (Ou autre httpd)
	* MySQL (Ou fork)
	* PHP


Les dépendances majeures
------------------------
* http://phppetitions.net/
* GIT
	* gitflow
		* http://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html
		* http://www.synbioz.com/blog/git-adopter-un-modele-de-versionnement-efficace
* vim	(Emacs est un très bon système d'exploitation auquel il ne manque qu'un
			bon éditeur de texte.)


Installation
------------
Voir le README du script.


Utilisation
-----------
Ce dépôt est géré par GIT. Pour le workflow je suis, scrupuleusement gitflow.

